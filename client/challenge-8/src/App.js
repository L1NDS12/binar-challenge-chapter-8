import './App.css';
import logo from './logo1.png';
import Register from './form/register';
import Edit from './form/edit';
import Show from './form/show';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {
  return (
    <Router>
      <Switch>
        <div className="App">

          <Route path="/" exact component={Home} />
          <Route path="/register" exact component={Register}/>
          <Route path="/edit" exact component={Edit}/>
          <Route path="/show" exact component={Show}/>
          <br/><br/>
          <ul className = "list">
            <Link to='/' className="list">
              <li>Home</li>
            </Link>
            <Link to='/register' className = "list">
              <li>Register</li>
            </Link>
            <Link to='/edit' className = "list">
              <li>Edit</li>
            </Link>
            <Link to='/show' className = "list">
              <li>Show</li>
            </Link>
          </ul>
        </div>
      </Switch>
    </Router>
  );
}
const Home = () =>(
  <div className="home">
    <h1> Home Page </h1>
    <br/>
    <img src={logo} alt="" srcset="" className="logo"/>
  </div>
)
export default App;
