import React from 'react';
import '../style/kedua.css';

function register() {
  return (
    <form action="" method="post" class="form">
        <h1 class="judul"> Register Page </h1>
        <div class="form-group">
            <label for="username">Username : </label>
            <input type="text" class="form-control" id="username" placeholder="Enter Your Username" name="username"/>
        </div>
        <div class="form-group">
            <label for="email">Email address : </label>
            <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" name="email"/>
        </div>
        <div class="form-group">
            <label for="password">Password : </label>
            <input type="email" class="form-control" id="password"  placeholder="Enter password" name="password"/>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  );
}

export default register;
