import React from 'react';
import '../style/pertama.css';

function show() {
  return (
    <form action="" method="post" class="form">
        <h1 class="judul"> Show Page </h1>
        <div class="form-group">
            <label for="username">Username : </label>
            <input type="text" class="form-control" id="username" placeholder="Enter Your Username" name="username"/>
        </div>
        <div class="form-group">
            <label for="email">Email address : </label>
            <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" name="email"/>
        </div>
        <div class="form-group">
            <label for="exp">Experience : </label>
            <input type="text" class="form-control" id="exp"  placeholder="Enter exp" name="password"/>
        </div>
        <div class="form-group">
            <label for="lvl">Level : </label>
            <input type="text" class="form-control" id="lvl"  placeholder="Enter level" name="password"/>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  );
}

export default show;
