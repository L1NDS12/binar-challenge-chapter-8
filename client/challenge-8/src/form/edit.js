import React from 'react';

import '../style/pertama.css';

function edit() {
  return (
    <form action="" method="post" class="form">
        <h1 class="judul"> Edit Page With Username : kepo1</h1>
        <div class="form-group">
            <label for="username">Username : </label>
            <input type="text" class="form-control" id="username" placeholder="Kepo1" name="username"/>
        </div>
        <div class="form-group">
            <label for="email">Email address : </label>
            <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" name="email"/>
        </div>
        <div class="form-group">
            <label for="exp">Experience : </label>
            <input type="text" class="form-control" id="exp"  placeholder="21" name="password" disabled/>
        </div>
        <div class="form-group">
            <label for="lvl">Level : </label>
            <input type="text" class="form-control" id="lvl"  placeholder="5" name="password" disabled/>
        </div>
        <br/><br/>
        <div class="alert alert-warning" role="alert">
          Hanya Username sama Email yang bisa diubah! 
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  );
}

export default edit;
