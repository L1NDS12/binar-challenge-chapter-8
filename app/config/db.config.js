// setup database here, change the values to suit your environment
module.exports = {
  HOST: "localhost",
  USER: "linds12",
  PASSWORD: "123456",
  DB: "database12",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
